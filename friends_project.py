"""
Should We Be Friends Project: PFE 1
Talitha Brobst; Fall 2023
"""
print("What is your name?")
name = str(input())
print("Hello", name,"!")

# declaring a storage variable for responses
score = 0
print("Should we be friends? \n(To answer a question with - YES - press 1) \n(To answer a question with - NO - press 2)\n")
# question 1
try:
    print("Question 1: Do you have a cat?")
    q1_cat = int(input())
    if q1_cat == 1:
        score = score + 10
        print("Nice! Me too!")
    elif q1_cat == 2:
        score = score - 10
        print("Aww that's too bad.")
    else:
        print("ERROR --- Please restart and only respond with 1 or 2")

# question 2
    print("\nQuestion 2: Do you enjoy sweet foods/desserts?")
    q2_sweet = int(input())
    if q2_sweet == 1:
        score = score + 10
        print("Same here :)")
    elif q2_sweet == 2:
        score = score - 10
        print("I guess you don't have much of a sweet tooth, huh?")
    else:
        print("ERROR --- Please restart and only respond with 1 or 2")
# question 3
    print("\nQuestion 3: Do you like to visit museums?")
    q3_museum = int(input())
    if q3_museum == 1:
        score = score + 15
        print("Cool! I was just at one this weekend.")
    elif q3_museum == 2:
        score = score - 15
        print("Not your thing? That's alright.")
    else:
        print("ERROR --- Please restart and only respond with 1 or 2")
# question 4
    print("\nQuestion 4: Do you enjoy watching sports?")
    q4_sports = int(input())
    if q4_sports == 1:
        score = score - 20
        print("It's not really my thing.")
    elif q4_sports == 2:
        score = score + 20
        print("Yeah, me neither.")
    else:
        print("ERROR --- Please restart and only respond with 1 or 2")
# question 5
    print("\nQuestion 5: Do you go out to parties often?")
    q5_parties = int(input())
    if q5_parties == 1:
        score = score - 15
        print("They're not my favorite.")
    elif q5_parties == 2:
        score = score + 10
        print("Same here.")
    else:
        print("ERROR --- Please restart and only respond with 1 or 2")
# question 6
    print("\nQuestion 6: Do you know your big three zodiac signs?")
    q6_zodiac = int(input())
    if q6_zodiac == 1:
        score = score + 20
        print("Nice! I have a Leo sun, Gemini moon, and Pisces rising.")
    elif q6_zodiac:
        score = score - 15
        print("Haha, fair enough.")
    else:
        print("ERROR --- Please restart and only respond with 1 or 2")
        # compatibility
    print("\nShould we be friends? \nANSWER:")
    comp_score = (score / 85) * 100
    print("We have a", comp_score, "percent compatibility.")
    if score >= 60:
        print("We would probably get along :)")
    elif 60 >= score >= 30:
        print("We could be acquaintances.")
    else:
        print("We don't have much in common :(")

except:
    print("ERROR --- Please restart and only respond with 1 or 2")