# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 11:52:56 2023
Encrypting Project
@author: tali
"""

encryption = {
    'A':'z',
    'a':'Z',
    'B':'y',
    'b':'Y',
    'C':'x',
    'c':'X',
    'D':'w',
    'd':'W',
    'E':'v',
    'e':'V',
    'F':'u',
    'f':'U',
    'G':'t',
    'g':'T',
    'H':'s',
    'h':'S',
    'I':'r',
    'i':'R',
    'J':'q',
    'j':'Q',
    'K':'p',
    'k':'P',
    'L':'o',
    'l':'O',
    'M':'n',
    'm':'N',
    'N':'m',
    'n':'M',
    'O':'l',
    'o':'L',
    'P':'k',
    'p':'K',
    'Q':'j',
    'q':'J',
    'R':'i',
    'r':'I',
    'S':'h',
    's':'H',
    'T':'g',
    't':'G',
    'U':'f',
    'u':'F',
    'V':'e',
    'v':'E',
    'W':'d',
    'w':'D',
    'X':'c',
    'x':'C',
    'Y':'b',
    'y':'B',
    'Z':'a',
    'z':'A',
    '1':'0',
    '2':'9',
    '3':'8',
    '4':'7',
    '5':'6',
    '6':'5',
    '7':'4',
    '8':'3',
    '9':'2',
    '0':'1',
    ' ':'#',
    ',':'/',
    '.':'*'
    }

print("""Welcome to your personal encryptor!
Please enter a phrase you would like to encrypt:""")
textToEncrypt = input(str())

newList = []

for letter in textToEncrypt:
    newList.append(encryption[letter])
    
## i found this section about joining in ch 8 of the book
## after failing to concatinate the way i wanted to
separator = ''
newText = separator.join(newList)

print('Great! Your encrypted phrase reads:')
print(newText)
