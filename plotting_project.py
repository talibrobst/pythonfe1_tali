# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 11:42:29 2023
plotting with Matplotlib
@author: tali
"""
import matplotlib.pyplot as plt
import numpy as np
# line plot
xL = np.array([2012,2013,2014,2015,2016,2017,2018,2019,2020,2021])
yL = np.array([22.6,22.8,15.5,10.7,34.2,28.5,27.8,32.9,32.3,35.9])
zL = np.array([13.1,9.3,6.6,16.1,10.5,7.4,9.6,13.2,14.2,4.9])
labelsL = ['UK Studio Backed', 'UK Independent']
font1L = {'family':'arial','size':18}
font2L = {'family':'arial','size':12}

plt.plot(xL,yL,'darkslategray',marker='D',ms=8,mec='k',mfc='darkcyan',ls=':',lw=2)
plt.plot(xL,zL,'darkmagenta',marker='*',ms=12,mec='k',mfc='darkorchid',ls=':',lw=2)
plt.xlabel('Year',fontdict=font2L)
plt.ylabel("Number of Shares",fontdict=font2L)
plt.title('UK films’ share of the UK theatrical market',fontdict=font1L)
plt.legend(title='Legend',labels=labelsL)
plt.show()

# scatterplot
xS = np.array([647,698,712,759,821,760,787,764,381,442])
yS = np.array([1.8,1.7,1.5,1.7,1.5,1.8,1.7,1.7,0.6,1.4])
font1S = {'family':'times new roman','size':14}
font2S = {'family':'times new roman','size':12}
font3S = {'family':'times new roman','size':12,'style':'oblique'}

plt.scatter(xS,yS,marker='X',c='mediumvioletred',edgecolors='k',s=70,)
plt.xlabel('Number of Releases',fontdict=font2S)
plt.ylabel('Average revenue per release (£ million)',fontdict=font2S)
plt.title('Revenues and releases at the UK and Republic of Ireland box office, 2012-2021',fontdict=font1S)
plt.text(647,1.8,'2012',va='bottom',ha='center',fontdict=font3S)
plt.text(698,1.7,'2013',va='bottom',ha='center',fontdict=font3S)
plt.text(712,1.5,'2014',va='bottom',ha='center',fontdict=font3S)
plt.text(759,1.7,'2015',va='top',ha='right',fontdict=font3S)
plt.text(821,1.5,'2016',va='bottom',ha='center',fontdict=font3S)
plt.text(760,1.8,'2017',va='bottom',ha='center',fontdict=font3S)
plt.text(787,1.7,'2018',va='top',ha='left',fontdict=font3S)
plt.text(764,1.7,'2019',va='bottom',ha='left',fontdict=font3S)
plt.text(381,0.6,'2020',va='bottom',ha='center',fontdict=font3S)
plt.text(442,1.4,'2021',va='bottom',ha='center',fontdict=font3S)
plt.show()

# bar graph

labelsB = np.array([13.7,12.2,11.4,16,16.6,13.9,18.7,15.6,11,16.3,12.2,18.5])
dataB = np.array(['Jan.','Feb.','Mar.','April','May','June','July','Aug.','Sept.','Oct.','Nov.','Dec.'])
colorsB = ['fuchsia','purple','plum','indigo','darkviolet','hotpink','darkmagenta','mediumvioletred','orchid','blueviolet','deeppink','rebeccapurple']
font1B = {'family':'serif','size':16}
font2B = {'family':'serif','size':12}

plt.bar(dataB,labelsB,width=.6,color=colorsB)
plt.xlabel('Months',fontdict=font2B)
plt.ylabel('Admissions (Million)',fontdict=font2B)
plt.title('Monthly UK Cinema Admissions in 2019',fontdict=font1B)
plt.show()

# histogram
yH = np.array([912.3,917,946,987,990,1027,1309,1494,1541,1575,1585,1635,1462,1514,1430,1395.8,1365,1312.1,1284.5,1275.8,1181.8,1100.8,915.2,754.7,581,500.8,449.1,395,357.2,342.8,326.6,288.8,264.8,237.3,214.9,193,176,156.6,134.2,138.5,116.3,103.9,103.5,126.1,111.9,101,86,64,65.7,54,72,75.5,78.5,84,94.5,97.4,100.3,103.6,114.4,123.5,114.6,123.5,138.9,135.2,139.1,142.5,155.9,175.9,167.3,171.3,164.7,156.6,162.4,164.2,173.5,169.2,171.6,172.5,165.5,157.5,171.9,168.3,170.6,177,176.1,44,74])
font1H = {'family':'cambria','size':16}
font2H = {'family':'cambria','size':12}

plt.hist(yH,bins=10,color='darkseagreen')
plt.xlabel('Admissions (million)',fontdict=font2H)
plt.ylabel('Frequency',fontdict=font2H)
plt.title('Annual UK admissions, 1935-2021',fontdict=font1H)
plt.show()

# pie chart

dataP = np.array([548,226,225,89,106,65,64,44,60,39,38,29,17,31,28])
labelsP = np.array(['China','USA','India','Russia','Japan','Mexico','France','UK','South Korea','Brazil','Germany','Spain','Poland','Indonesia','Italy'])
font1P = {'family':'georgia','size':18}
colorsP = ['crimson','red','orangered','darkorange','orange','gold','yellow','yellowgreen','forestgreen','teal','royalblue','slateblue','blueviolet','darkviolet','mediumvioletred']

plt.pie(dataP,colors=colorsP,counterclock=False)
plt.legend(title='Legend',labels=labelsP,bbox_to_anchor=(1,1), loc='upper left', borderaxespad=0)
plt.title('Largest Markets by Admissions 2020',fontdict=font1P)
plt.show()
