"""
Critical Parameter Guessing Project
Talitha Brobst - PFE1 Fall 2023
"""
import random as rand

# the program will ask the user for their height and attempt to guess it within the provided range
print("Hello there! Please enter your height in inches so that the computer can try to guess it.")
user_height_inch = float(input())
user_height_meter = user_height_inch * 0.0254
print("Great! You are", user_height_inch, "inches tall (which is equal to", user_height_meter, "meters tall).\n")
print("How close must the computer's guess be to your actual height?")
prox_height = float(input())

counter = 1
top_range = user_height_inch + prox_height
bottom_range = user_height_inch - prox_height
guess = 0

while guess < bottom_range or guess > top_range:
    guess = rand.randrange(25, 108)
    print("Guess #", counter, ": ", guess)
    counter = counter + 1
print("*****************************************************")
print("SUCCESS! The computer guessed", guess, "inches on Guess #", counter, ".")
closeness = user_height_inch - guess
print("The computer was", closeness, "inches away from guessing your height.")
