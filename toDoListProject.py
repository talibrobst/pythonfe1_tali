# -*- coding: utf-8 -*-
"""
Created on Wed Oct 25 10:56:52 2023
To Do List Project
@author: tali
"""

# purpose of code 
    # create an interactive to do list for the user to view and edit
    
### project extensions included: duplication prevention ###
    
#creating the to do list
toDoList = ['water the plants','feed the spider','sweep the attic','study the bones','light the candles'] 

#allows program to run until user stops it by making userRun = False
userRun = True
userInput = 0

while userRun: 
# user instructions/main menu
    if userInput == 0:
        print("""***************
Welcome to your To Do List! :)
  What would you like to do today?
    1. View your To Do List
    2. Delete a task from your list
    3. Add a task to your list
    4. Exit Program
***************""")
        userInput = int(input())
        
# option 1
    if userInput == 1:
        number = 0
        print("""***************
Your To Do List:""")
        for item in toDoList:
            number = number + 1
            print(number,"...", item)
        print("""
*** to return to the main menu, press [0] ***""")
        userInput = int(input())

# option 2
    if userInput == 2:
        print("""***************
You have selected the delete option, which step on your list would you like to delete?""")
        userDelete = int(input())
        userDelete = userDelete - 1
        del toDoList[userDelete]
        print('Ok, done!')
        userInput = 0
        
# option 3
    if userInput == 3:
        print("""***************
You have selected to add a step, what would you like to add to your list?""")
        userAdd = str(input())
        if userAdd in toDoList:
            print("""*** ERROR, CANNOT BE ADDED ***
(That step is already in your To Do List.)""")
            userInput = 0
        else:
            toDoList.append(userAdd)
            print('Ok, it has been added!')
            userInput = 0
            
# option 4
    if userInput == 4:
        print("""***************
end of program, goodbye!""")
        userRun = False

        