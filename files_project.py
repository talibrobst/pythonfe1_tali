# -*- coding: utf-8 -*-
"""
Created on Mon Nov 13 11:40:37 2023
Files Project
@author: tali
"""

seqH1 = 'ABC123'
seqD1 = 'ATCGTACGATCGATCGATCGCTAGACGTATCG'
seqH2 = 'DEF456'
seqD2 = 'actgatcgacgatcgatcgatcacgact'
seqH3 = 'HIJ789'
seqD3 = 'ACTGACtACTGTctACTGTAccttCATGTG'
seqH4 = 'XYZ321'
seqD4 = 'ACTGAC---ACTGTA-CTGTA----CATGTG'

seqD1 = seqD1.upper()
seqD2 = seqD2.upper()
seqD3 = seqD3.upper()
seqD4 = seqD4.upper().replace('-','')

fastaFile = open('FASTA_sequences.txt','w')
# sequence 1
fastaFile.write('>')
fastaFile.write(seqH1)
fastaFile.write('\n')
fastaFile.write(seqD1)
fastaFile.write('\n')
# sequence 2
fastaFile.write('>')
fastaFile.write(seqH2)
fastaFile.write('\n')
fastaFile.write(seqD2)
fastaFile.write('\n')
# sequence 3
fastaFile.write('>')
fastaFile.write(seqH3)
fastaFile.write('\n')
fastaFile.write(seqD3)
fastaFile.write('\n')

fastaFile.close()

xyzFasta = open('XYZ321.fasta','w')
# sequence 4
xyzFasta.write('>')
xyzFasta.write(seqH4)
xyzFasta.write('\n')
xyzFasta.write(seqD4)

xyzFasta.close()